package signutil

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"golang.org/x/crypto/sha3"
)

type SignatureData struct {
	Signature string `json:"sig"`
	Checksum  string `json:"checksum"`
}

var (
	regex = "(?s)--- BEGIN SIGNED MESSAGE ---\n" +
		"(?P<message>.*?)\n" +
		"--- END SIGNED MESSAGE ---" +
		".*(?P<json>{.*?})"
	re = regexp.MustCompile(regex)

	ErrTooManyBlocks   = errors.New("too many or no encoded messages")
	ErrIncorrectFormat = errors.New("incorrect message formatting")
	ErrChecksumBad     = errors.New("checksum mismatch")
)

func VerifySig(sigHex string, msg []byte) (string, error) {
	sig, err := hexutil.Decode(sigHex)
	if err != nil {
		return "", err
	}
	msg = accounts.TextHash(msg)
	if sig[crypto.RecoveryIDOffset] > 1 {
		sig[crypto.RecoveryIDOffset] -= 27
	}
	recovered, err := crypto.SigToPub(msg, sig)
	if err != nil {
		return "", err
	}

	recoveredAddr := crypto.PubkeyToAddress(*recovered)
	return recoveredAddr.Hex(), nil
}

func GetOriginator(messageText string) ([]byte, error) {
	ex, err := ExtractMessage(messageText)
	if err != nil {
		return nil, err
	}

	formattedHash, err := Checksum(ex.message)
	if err != nil {
		return nil, err
	}

	if FormatBytes(formattedHash) != ex.meta.Checksum {
		return nil, ErrChecksumBad
	}

	signer, err := VerifySig(ex.meta.Signature, []byte(ex.message))
	if err != nil {
		return nil, err
	}
	return HexToBytes(signer)
}

func HexToBytes(hv string) ([]byte, error) {
	return hex.DecodeString(strings.TrimPrefix(hv, "0x"))
}

func FormatBytes(bytes []byte) string {
	return fmt.Sprintf("0x%s", hex.EncodeToString(bytes))
}

func Checksum(messageText string) ([]byte, error) {
	k256 := sha3.NewLegacyKeccak256()
	_, err := k256.Write([]byte(messageText))
	if err != nil {
		return nil, err
	}
	keccakHash := k256.Sum(nil)
	return keccakHash, nil
}

type extractedMessage struct {
	message string
	meta    *SignatureData
}

func ExtractMessage(messageText string) (*extractedMessage, error) {
	result := make(map[string]string)
	matches := re.FindAllStringSubmatch(messageText, -1)
	if len(matches) != 1 {
		return nil, ErrTooManyBlocks
	}
	if len(matches[0]) != 3 {
		return nil, ErrIncorrectFormat
	}
	for i, name := range re.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = matches[0][i]
		}
	}
	var jsonData SignatureData
	err := json.Unmarshal([]byte(result["json"]), &jsonData)
	if err != nil {
		return nil, err
	}

	return &extractedMessage{
		message: result["message"],
		meta:    &jsonData,
	}, nil
}
