package web3

import (
	"context"
	"encoding/hex"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/ethereum/go-ethereum/common"
)

func TestSign(t *testing.T) {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			t.Fatal(err)
		}
		if string(b) == "{\"method\":\"eth_sign\",\"params\":[\"0x0123456789abcdef0123456789abcdef12345678\",\"68656c6c6f\"],\"id\":0,\"jsonrpc\":\"2.0\"}" {
			w.Write([]byte("{\"id\":1,\"jsonrpc\":\"2.0\",\"result\":\"0x0123456789abcdef0123456789abcdef12345678\"}"))
		}
	}))

	prov := NewProvider(svr.URL, "TestEth")
	address := common.HexToAddress("0x0123456789abcdef0123456789abcdef12345678")
	addrs, err := prov.Sign(context.Background(), address, []byte("hello"))
	if err != nil {
		t.Fatal(err)
	}

	expected, err := hex.DecodeString("0123456789abcdef0123456789abcdef12345678")
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(addrs, expected) {
		t.Fatalf("Expected %v got %v", expected, addrs)
	}
}
