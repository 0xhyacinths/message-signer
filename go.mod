module gitlab.com/0xhyacinths/signcli

go 1.18

require (
	github.com/bwmarrin/discordgo v0.25.0
	github.com/ethereum/go-ethereum v1.10.19
	github.com/spf13/cobra v1.5.0
	github.com/ybbus/jsonrpc/v3 v3.1.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)

require (
	github.com/btcsuite/btcd/btcec/v2 v2.2.0 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.0.1 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
)
