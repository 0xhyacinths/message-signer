package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/0xhyacinths/signcli/lib/signutil"
)

var token string

func init() {
	flag.StringVar(&token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	if token == "" {
		fmt.Println("No token provided.")
		return
	}

	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("Error creating Discord session: ", err)
		return
	}

	dg.AddHandler(ready)
	dg.AddHandler(messageCreate)

	dg.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildMessages

	err = dg.Open()
	if err != nil {
		fmt.Println("Error opening Discord session: ", err)
	}

	fmt.Println("EthSigVerify running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	dg.Close()
}

func ready(s *discordgo.Session, event *discordgo.Ready) {
	fmt.Println("ready")
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.Contains(m.Content, "--- BEGIN SIGNED MESSAGE ---") {
		originAddress, err := signutil.GetOriginator(m.Content)
		embed := &discordgo.MessageEmbed{
			Author:      &discordgo.MessageEmbedAuthor{},
			Color:       0xFF5733, // red
			Description: "Signature verification failed",
			Fields:      []*discordgo.MessageEmbedField{},
			Timestamp:   time.Now().Format(time.RFC3339),
			Title:       "Signature Check",
		}
		// be safe, the embed is by default a failure condition.
		if err == nil {
			addr := signutil.FormatBytes(originAddress)
			explorerUri := "https://etherscan.io/address"
			link := fmt.Sprintf("[%s](%s/%s)", addr, explorerUri, addr)
			embed.Color = 0x50C878 // green
			embed.Fields = append(embed.Fields,
				&discordgo.MessageEmbedField{
					Name:   "Signer Address",
					Value:  link,
					Inline: true,
				})
			embed.Description = "Signature verification passed"
		}
		s.ChannelMessageSendEmbed(m.ChannelID, embed)
	}
}
