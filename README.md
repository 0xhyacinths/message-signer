# Discord Signer Bot Thing
A lot of crypto projects rely heavily on their Discord servers for community.
However, as a result, they're a fairly large target for compromise and scams.

This tool hopes to combat that. 

***Please be careful signing arbitrary messages! This tool uses the [ERC-191][1]
signing scheme which should prevent them being used as malicious transactions,
but message signatures on the same content can potentially be reused.***

## Bot
You can run your own signer bot, whether this is the right solution for you or
if you should use another hosted instance is dependent on your situation. If you
can correctly secure your own infrastructure then maybe that's a good idea.

However, if you can't, the bot being compromised could be used to instill false
trust in forged messages. Users should do their due diligence and verify the
signature of a message as well. 

Avoiding having a single point of failure is a feature, if one instance gets
compromised, the blast radius would be contained. 

Running the bot is fairly easy.

```bash
$ signbot -t <discord bot token>
```

## CLI 
The CLI utility has two modes. Create and Verify.

### Creating signatures
- Write your message in a text file
- Run `signcli sign <path-to-text-file>`
- Sign the message
- Copy the output and paste it into your Discord server.

Please avoid using markdown formatting in messages because it's not easy to copy
and paste messages with formatting, making verification by other users a little
more difficult.

### Verifying signatures
- Copy and paste the signed message into a text file
- Run `signcli verify <path-to-text-file>`

The output should print whether the signature is valid, and if so, the signer's
address.

[1]: https://eips.ethereum.org/EIPS/eip-191
