package main

import (
	"log"

	"github.com/spf13/cobra"
)

var (
	providerUri string
)

var RootCmd = &cobra.Command{
	Use:   "signcli",
	Short: "The root of signcli",
}

func main() {
	if err := RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	RootCmd.PersistentFlags().StringVar(&providerUri, "provider",
		"http://127.0.0.1:1248",
		"provider URI (default is http://127.0.0.1:1248)")
}
