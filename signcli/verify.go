package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/0xhyacinths/signcli/lib/signutil"
)

var verifyCmd = &cobra.Command{
	Use:   "verify",
	Short: "Verify an encoded message",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("you need to pass a filename")
		}

		fp, err := filepath.Abs(args[0])
		if err != nil {
			return err
		}

		dat, err := os.ReadFile(fp)
		if err != nil {
			return err
		}

		inputText := string(dat)

		originator, err := signutil.GetOriginator(inputText)
		if err != nil {
			return err
		}

		fmt.Println("Message signature validated")
		fmt.Printf("Originator: %s\n", signutil.FormatBytes(originator))

		return nil
	},
}

func init() {
	RootCmd.AddCommand(verifyCmd)
}
