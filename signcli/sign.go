package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/0xhyacinths/signcli/lib/signutil"
	"gitlab.com/0xhyacinths/signcli/lib/web3"
	"golang.org/x/crypto/sha3"
)

var signCmd = &cobra.Command{
	Use:   "sign",
	Short: "Sign a encoded message",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("you need to pass a filename")
		}

		fp, err := filepath.Abs(args[0])
		if err != nil {
			return err
		}

		dat, err := os.ReadFile(fp)
		if err != nil {
			return err
		}

		inputText := strings.TrimSpace(string(dat))

		w3 := web3.NewProvider(providerUri, "SignCLI")
		accounts, err := w3.Accounts(context.Background())
		if err != nil {
			return err
		}

		if len(accounts) == 0 {
			return errors.New("no accounts found")
		}

		var sb strings.Builder
		sb.WriteString("--- BEGIN SIGNED MESSAGE ---\n")
		sb.WriteString(inputText)
		sb.WriteString("\n--- END SIGNED MESSAGE ---\n\n```json\n")

		k256 := sha3.NewLegacyKeccak256()
		_, err = k256.Write([]byte(inputText))
		if err != nil {
			return err
		}
		hash := k256.Sum(nil)

		sig, err := w3.Sign(context.Background(), accounts[0], []byte(inputText))
		if err != nil {
			return err
		}

		sd := &signutil.SignatureData{
			Checksum:  signutil.FormatBytes(hash),
			Signature: signutil.FormatBytes(sig),
		}

		enc, err := json.MarshalIndent(sd, "", " ")
		if err != nil {
			return err
		}
		sb.WriteString(string(enc))
		sb.WriteString("\n```\n")
		fmt.Println(sb.String())
		return nil
	},
}

func init() {
	RootCmd.AddCommand(signCmd)
}
